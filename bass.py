# Pulse 110.092 - bass.py
#
# Copyright (C) 2021 Guillaume Tucker <guillaume.tucker@gmail.com>
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import splat.data
import splat.filters
import splat.gen
import splat.interpol
import splat.scales
import splat.seq
import splat.tools.make
from splat import lin2dB, dB2lin as dB

import settings


class BassGenerator(splat.gen.OvertonesGenerator):

    def __init__(self, levels, *args, **kw):
        super().__init__(*args, **kw)
        self.levels = levels
        self.filters = self._make_env(settings.PERIOD)

    def _make_env(self, period):
        spline = splat.interpol.spline([
            (0.0, dB(0.0)),
            (period/4, dB(2.5), 0.0),
            (period/2, dB(-6.0), 0.0),
            (period, dB(-100.0)),
        ])
        env = splat.data.Fragment(duration=period, channels=1)
        env.offset(1.0)
        env.amp(spline.signal)
        return [
            (splat.filters.linear_fade, (0.04,)),
            (lambda frag, e: frag.amp(e), (env,)),
        ]


class Bass(splat.seq.Pattern):
    PARAMS = [
        (1.0, 1.8, dB(0)),
        (2.5, 0.8, dB(-15.0)),
        (4.0, 2.1, dB(-43.0)),
    ]

    def __init__(self, notes, levels=dB(0), *args, **kw):
        super().__init__(*args, **kw)
        self._scale = splat.scales.LogScale(key='E')
        self._freqs = [
            self._scale[note] if note else None for note in notes
        ]
        self._gens = self._make_generators()

    def _make_generators(self):
        return [
            (BassGenerator(lvl), exp, octave)
            for octave, exp, lvl in self.PARAMS
        ]

    def _update_generator(self, gen, exp, t):
        gen.ot_decexp(exp)

    def play(self, frag, bar, beat, t, T):
        freq = self._freqs[beat]
        if freq is None:
            return
        t -= T * 0.045
        for gen, exp, octave in self._gens:
            self._update_generator(gen, exp, t)
            gen.frag = frag
            gen.run(t, t + T * 0.3, freq * octave)


class BassIntro(Bass):
    RAMP = [
        (0,  0.0, 0.0),
        (4,  0.3),
        (8,  0.8, 0.1),
        (16, 1.0, 0.0),
    ]

    def _make_generators(self):
        gens = list()
        for octave, exp, levels in self.PARAMS:
            ramp = list()
            for point in self.RAMP:
                ramp_point = [
                    point[0] * settings.PERIOD,
                    point[1] * exp,
                ]
                if len(point) > 2:
                    ramp_point.append(point[2])
                ramp.append(tuple(ramp_point))
            exp_spline = splat.interpol.spline(ramp)
            gens.append((BassGenerator(levels), exp_spline, octave))
        return gens

    def _update_generator(self, gen, exp, t):
        gen.ot_decexp(exp.value(t))


def run(argv):
    frag = splat.data.Fragment(rate=settings.RATE)

    # Chords: Em Em F7 F7
    notes1 = ['E-4', 'E-4', 'F-4', 'Eb-5']
    # Chords: C7M C7M
    notes2a = ['C-5'] * 4
    # Chords: F#dim F7
    notes2b = ['A-4', 'A-4', 'F-4', 'F-4']
    # Chords: F79 F79b
    notes2c = ['F-4', 'Eb-5', 'F-4', 'Eb-5']
    # Chords: Bbm Bbm (or Bbm F7)
    notes3 = ['Bb-4', 'Bb-4', 'F-4', 'Eb-5']

    padding = splat.seq.Silence(beats=1)
    B0 = BassIntro(notes1)
    B1 = Bass(notes1)
    B1x = Bass(notes1[:3] + [None])
    B2a = Bass(notes2a)
    B2b = Bass(notes2b)
    B2c = Bass(notes2c)
    B3 = Bass(notes3)

    intro = [padding] + [B0] * 4 + [B1] * 6 + [B1, B1x]
    part1 = [B1] * 6
    part1a = [B2a, B2b, B1, B1, B2a, B2b]
    part1b = [B1] * 4 + [B2c] * 2
    part2 = [B3] * 12

    seq = (
         intro +
         part1 + part1b + (
             part1 + part1a
         ) * 2 +
         part1 + part1b +
         part2 + (
             part1 + part1a
         ) * 2
    )

    splat.seq.PatternSequencer(settings.TEMPO).run(frag, seq)
    env = splat.interpol.spline([
        (0.0, 0.0),
        (settings.PERIOD, 0.0, 0.0),
        (13 * settings.PERIOD, 1.0, 0.0),
        (frag.duration, 1.0, 0.0),
    ])
    frag.amp(env.signal)
    return frag


if __name__ == '__main__':
    splat.tools.make.main_frag(sys.argv, run)
