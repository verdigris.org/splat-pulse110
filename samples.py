# Pulse 110.092 - samples.py
#
# Copyright (C) 2021 Guillaume Tucker <guillaume.tucker@gmail.com>
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

import splat.data
import splat.filters
import splat.seq
from splat import lin2dB, dB2lin as dB

import settings


SAMPLE_FILES = {
    'kick': "kick.wav",
    'snare': "snare.wav",
    'hi-hat-open': "hi-hat-open.wav",
}


def load():
    samples = dict()
    for name, path in SAMPLE_FILES.items():
        sample_path = os.path.join(settings.SAMPLES_PATH, path)
        samples[name] = splat.data.Fragment.open(sample_path)
    SAMPLES = samples
    return samples


SAMPLES = load()


class BarSample(splat.seq.Pattern):
    def __init__(self, sample, offset=0.0, levels=None, *args, **kw):
        super().__init__(*args, **kw)
        self._sample = sample
        self._offset = offset
        self._levels = levels or dB(0)

    def play(self, frag, bar, beat, t, T):
        if beat == 0:
            frag.mix(self._sample, t + self._offset, levels=self._levels)


class SamplePattern(splat.seq.Pattern):
    def __init__(self, sample_map, levels=None, *args, **kw):
        """sample_map: {
          (bar, beat): (frag, offset, levels),
        }

        Global levels are applied on top of optional sample levels (if
        not None).  If bar or beat are None, they get played every
        time (e.g. every bar and/or every beat).
        """
        super().__init__(*args, **kw)
        self._sample_map = sample_map
        self._levels = levels or dB(0)

    def play(self, frag, bar, beat, t, T):
        samples = [self._sample_map.get(x) for x in [
            (bar, beat), (bar, None), (None, beat), (None, None)
        ]]
        for sample in samples:
            if sample:
                sample_frag, offset, levels = sample
                if levels is None:
                    levels = 1.0
                    levels *= self._levels
                if offset is None:
                    offset = 0.0
                frag.mix(sample_frag, t + offset, levels=levels)


def get(name):
    return SAMPLES.get(name)


def make_drum_samples():
    return {name: SAMPLES[name] for name in [
        'kick', 'snare', 'hi-hat-open',
    ]}
