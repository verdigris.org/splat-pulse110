# Pulse 110.092 - pulse.py
#
# Copyright (C) 2021 Guillaume Tucker <guillaume.tucker@gmail.com>
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import os
import sys

import splat
import splat.data
import splat.interpol
import splat.tools.make
from splat import lin2dB, dB2lin as dB

import settings

SPLAT_DEPS = ['settings.py', 'samples.py']

SPLAT_FRAGS = {
    'drums': (0.0, dB(0)),
    'bass': (0.0, dB(-11)),
}


def main(args):
    print(f"Track name: {settings.NAME}")
    track = splat.data.Fragment(rate=settings.RATE)
    splat.tools.make.main_mixer(sys.argv, SPLAT_FRAGS, track)


if __name__ == '__main__':
    main(sys.argv)
