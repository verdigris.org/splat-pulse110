# Pulse 110.092 - drums.py
#
# Copyright (C) 2021 Guillaume Tucker <guillaume.tucker@gmail.com>
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import splat.data
import splat.seq
import splat.tools.make
from splat import lin2dB, dB2lin as dB

import settings
import samples
from samples import SamplePattern


def run(argv):
    frag = splat.data.Fragment(rate=settings.RATE)
    drum_samples = samples.make_drum_samples()
    padding = splat.seq.Silence(beats=1)
    kick, snare, hat = (drum_samples[name] for name in [
        'kick', 'snare', 'hi-hat-open'
    ])

    Intro = SamplePattern({
        (None, 0): (kick, None, None),
        (None, 2): (kick, None, None),
    })

    Kick1 = SamplePattern({
        (None, 0): (kick, None, None),
        (None, 1): (kick, None, dB(-9)),
        (None, 2): (kick, None, None),
        (None, 3): (kick, None, dB(-9)),
    })
    Kick1b = SamplePattern({
        (None, 0): (kick, settings.PERIOD/2, dB(-3)),
        (None, 1): (kick, settings.PERIOD/2, dB(-12)),
        (None, 2): (kick, settings.PERIOD/2, dB(-3)),
        (None, 3): (kick, settings.PERIOD/2, dB(-9)),
    })
    Kick1c = SamplePattern({
        (None, 0): (kick, settings.PERIOD/2, dB(-3)),
        (None, 1): (kick, settings.PERIOD/2, dB(-12)),
        (None, 2): (kick, settings.PERIOD/2, dB(-3)),
    })
    Snare1 = SamplePattern({
        (None, 1): (snare, None, dB(-7)),
        (None, 3): (snare, None, dB(-8)),
    })

    Kick2 = SamplePattern({
        (None, 0): (kick, None, None),
        (None, 1): (kick, None, dB(-9)),
        (None, 2): (kick, None, None),
    })
    Kick2b = SamplePattern({
        (None, 0): (kick, settings.PERIOD/2, dB(-3)),
        (None, 1): (kick, settings.PERIOD/2, dB(-12)),
    })
    Snare2 = SamplePattern({
        (None, 1): (snare, None, dB(-5)),
    })

    Char1 = SamplePattern({
        (None, 2): (hat, settings.PERIOD / 2, dB(-18)),
    })
    Char2 = SamplePattern({
        (None, 0): (hat, settings.PERIOD / 2, dB(-18)),
    })

    S1 = [Kick1, Snare1, Char1]
    S2 = [Kick1, Kick1c, Char2]

    S = splat.seq.Silence()

    intro_a = [Intro, [Intro, Char1]]
    intro_b = [Kick1, Kick1, Kick1, [Kick1, Char1]] * 2
    intro_c = [[Kick1, Kick1b], [Kick2, Kick2b]]

    intro = [padding] + intro_a + intro_b + intro_c
    part1 = ([S1] * 4 + [S1, S2]) * 2
    part2 = ([S1] * 5 + [S2]) * 2

    seq = (
        intro +
        part1 * 4 +
        part2 +
        part1 * 2
    )

    splat.seq.PatternSequencer(settings.TEMPO).run(frag, seq)
    return frag


if __name__ == '__main__':
    splat.tools.make.main_frag(sys.argv, run)
