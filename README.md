## Pulse 110.092

This repository contains the source code and audio samples to generate the
drums and bass parts for [Pulse 110.092](https://verdigris.org/article/39).

To use it, first install [Splat](https://gitlab.com/gtucker/splat).  Then run:
```
splat make pulse
```

It should generate a `splat.wav` file.

The drum samples were imported from [Hydrogen](http://hydrogen-music.org/)'s
UltraAcousticKit.
